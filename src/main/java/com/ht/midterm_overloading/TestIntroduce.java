/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.midterm_overloading;

/**
 *
 * @author ACER
 */
public class TestIntroduce {
    public static void main(String[] args) {
        Introduce per1 = new Introduce("World",20,20,20);
        per1.say();
        System.out.println("-----------------------------");
        per1.say("Girl", 9);
        System.out.println("-----------------------------");
        per1.say(32, 1.31);
        System.out.println("-----------------------------"); 
        per1.say("Boy", 10, 32, 1.31);
        System.out.println("-----------------------------"); 
        per1.say("Lill", 10, 40, 0);
        
    }
}
