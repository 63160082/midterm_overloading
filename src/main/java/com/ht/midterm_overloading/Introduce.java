/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.midterm_overloading;

/**
 *
 * @author ACER
 */
public class Introduce {  
    private String name;
    private int age;
    private double weight; //หน่วยเป็น kg
    private double height; //หน่วยเป็น m

    public Introduce(String name, int age, double weight, double height) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.height = height;
    }
    
    public double calBMI(double weight , double height) {
        if(weight <= 0 || height <= 0) {
            System.out.println("Weight or Height must more than 0");
        }
        return weight / (height * height);
    }
    
    // overloading - ชื่อ method เหมือนกัน ต่างกันที่ parameter ทีรับเข้า
    
    public void say() {   
        System.out.println("Hello World");
    }
    
    public void say(String name , int age) {
        System.out.println("I'm " + name + " I'm " + age + " years old.");
    }
    
    public void say(double weight , double height) {
        System.out.println("Weight : " + weight + " kg. , Height : " + height + " m. BMI is " + calBMI(weight,height));
    }
    
    public void say(String name, int age, double weight, double height) {
        System.out.println("I'm " + name + " I'm " + age + " years old , Weight : " + weight + " kg. , Height : " + height + " m. BMI is " + calBMI(weight,height));
    }
    
}
